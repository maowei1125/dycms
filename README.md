<div align="center">
<img src="./public/static/dycms/images/logo2.png" width="250">
    <h1>DYcms 2.0</h1>
    <p>Dycms - 人人都是生活的导航者</p>
      <a href="https://github.com/dongyao8/dycms/releases"><img src="https://img.shields.io/github/v/release/dongyao8/dycms.svg?logo=git&" alt="Release-Version"></a>
      <a href="https://github.com/dongyao8/dycms/blob/master/LICENSE"><img alt="GitHub license" src="https://img.shields.io/github/license/dongyao8/dycms"></a>
      <a href="https://github.com/dongyao8/dycms"><img src="https://img.shields.io/badge/Laravel-V9.x+-ff2c1f.svg?logo=laravel" alt="Laravel"></a>
      <a href="https://gitee.com/dongyao/dycms"><img src="https://img.shields.io/badge/Gitee-码云-CC3333.svg?logo=gitee" alt="Gitee"></a>
      <a><img src="https://img.shields.io/badge/PHP-v8.0+-5a78b7.svg?logo=PHP" alt="php版本"></a>
    <br>
    <a href="https://www.dongyao.ren"><img src="https://img.shields.io/badge/author-clark-27c4f2.svg?logo=github" alt="Author"></a>
     <a href="https://weibo.com/u/21376252"><img src="https://img.shields.io/badge/微博-21376252-ff8200.svg?logo=Sina Weibo" alt="微博"></a>

</div>  

>  `DYCMS`前身是一款集网址导航，内容发布，用户管理等于一体的综合性网站平台，随着业务功能的迭代，索性2.X版本以后，将该平台定为一个全功能型框架，特此升级本套代码，方便各位用于自身需要，或者运营自己的业务使用。

> 随着2.0版本的升级，DYCMS已经不仅仅可以实现网址导航，后续采用模块化结构之后，DYCMS也可以变身博客，企业网站，电商，APP，小程序等等全能型后台架构。

版本库：[ [gitee](https://gitee.com/dongyao/dycms) | [github](https://github.com/dongyao8/dycms)]

> 使用文档：[2.0文档（完善中）](http://docs.dongyao.ren/dycms-docs)
> 2.0演示网站：(http://ai.dongyao.ren/nav)

## 后台部分展示
> 前端页面当前正在招募合作伙伴一起完善，欢迎推荐加入！

<img src="./public/demo/1.jpg" width="80%">
<img src="./public/demo/2.jpg" width="80%">

## 后续开发计划

<!-- - [✓] 博客模块 -->
- [ ] 博客模块
- [ ] [请点击提交issues](https://github.com/dongyao8/dycms/issues)

## 感谢Star

[![Stargazers over time](https://starchart.cc/dongyao8/dycms.svg)](https://starchart.cc/dongyao8/dycms)
![Alt](https://repobeats.axiom.co/api/embed/0b185dce97f94fc36517abf5695457f822d402f0.svg "Repobeats analytics image")


### 官方QQ群：

- DYCMS交流1群 `648120877` 【已满员】
- DYCMS交流2群 `778957856`
> 非常感谢您关注和支持选择DYCMS ^\_^

## 关注微信

不定时更新最新动态，获取最新讯息。

![](./public/static/dycms/images/qrcode.jpg)

## 友情赞助
> 如果您觉得这个平台对你有帮助，可以通过下方二维码打赏支持一下！

<img src="./public/static/dycms/images/wechat.jpg" width="250">
<img src="./public/static/dycms/images/alipay.jpg" width="250">


## 免责声明

DYcms程序是免费开源的产品，仅用于学习交流使用！       
不可用于任何违反`中华人民共和国(含台湾省)`或`使用者所在地区`法律法规的用途。      
因为作者即本人仅完成代码的开发和开源活动`(开源即任何人都可以下载使用)`，从未参与用户的任何运营和盈利活动。    
且不知晓用户后续将`程序源代码`用于何种用途，故用户使用过程中所带来的任何法律责任即由用户自己承担。

## License

Apache License 2.0 [Apache License](https://github.com/dongyao8/dycms/blob/master/LICENSE).
